{
  "actors": [
    {
      "id": "6291d91a-a11b-43d2-a3d6-9273f069f3a3",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 191,
      "y": 291,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
          "text": "Serviços publicitados",
          "type": "istar.Goal",
          "x": 442,
          "y": 383,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d01ea420-83d2-440a-80ef-065de95ea200",
          "text": "Registar conta",
          "type": "istar.Task",
          "x": 428,
          "y": 459,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 568,
          "y": 432,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
          "text": "Validar conta",
          "type": "istar.Task",
          "x": 426,
          "y": 556,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
          "text": "Animal acolhido",
          "type": "istar.Goal",
          "x": 695,
          "y": 533,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d3d860a6-571f-496a-97f9-42c29f04aa85",
          "text": "Receber animal",
          "type": "istar.Task",
          "x": 780,
          "y": 363,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
          "text": "Introduzir NIF",
          "type": "istar.Task",
          "x": 426,
          "y": 642,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
          "text": "Serviço pedido",
          "type": "istar.Goal",
          "x": 695,
          "y": 380,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
          "text": "Receber pagamento",
          "type": "istar.Task",
          "x": 809,
          "y": 433,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
          "text": "Preencher declaração",
          "type": "istar.Task",
          "x": 571,
          "y": 532,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
          "text": "Animal entregue",
          "type": "istar.Goal",
          "x": 819,
          "y": 636,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8",
          "text": "Aumentar receita",
          "type": "istar.Goal",
          "x": 558,
          "y": 291,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "6422aaa1-581d-42be-b052-780206d80ead",
          "text": "Aumentar Alcance",
          "type": "istar.Quality",
          "x": 265,
          "y": 368,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
          "text": "Receber Critica Positiva",
          "type": "istar.Task",
          "x": 191,
          "y": 504,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
          "text": "Receber Critica Negativa",
          "type": "istar.Task",
          "x": 317,
          "y": 502,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "text": "API Website PetSitting",
      "type": "istar.Agent",
      "x": 439,
      "y": 839,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "text": "Cuidador Particular",
      "type": "istar.Role",
      "x": 116,
      "y": 116,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "text": "Empresa",
      "type": "istar.Role",
      "x": 394,
      "y": 92,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "f8755a51-5bc3-4827-bf0b-67683cc1fc07",
      "text": "Dono de animal",
      "type": "istar.Role",
      "x": 1041,
      "y": 645,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "76a24f3d-daaa-47ea-8376-8d033245d33b",
          "text": "Pesquisar Cuidadores",
          "type": "istar.Task",
          "x": 1101,
          "y": 729,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "232176fc-5cfd-47eb-8739-1c45e85af606",
          "text": "Pesquisar por multifiltro",
          "type": "istar.Task",
          "x": 1042,
          "y": 821,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7b65a7b0-7876-4bd2-ad21-124ed8867e87",
          "text": "Cuidador encontrado",
          "type": "istar.Goal",
          "x": 1103,
          "y": 667,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "3f121ca9-c712-4917-8625-d92e4b879097",
          "text": "Animal Entregue",
          "type": "istar.Goal",
          "x": 1331,
          "y": 790,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "88ae1a01-7319-4519-ac20-658e964728e7",
          "text": "Animal Recebido",
          "type": "istar.Goal",
          "x": 1413,
          "y": 922,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7db97c1c-8d6e-47a6-a900-24a96bf256ad",
          "text": "Comunicar com Cuidadores",
          "type": "istar.Task",
          "x": 1171,
          "y": 821,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "104f892f-54c8-47d5-84fe-b45ab3b1126d",
          "text": "Preecher Formulário de Registo",
          "type": "istar.Task",
          "x": 1229,
          "y": 902,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "17f40edb-4ab1-45d8-a82a-cc9a5c5bd778",
          "text": "Marcar Serviço",
          "type": "istar.Task",
          "x": 1104,
          "y": 900,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7029f750-67f0-4468-ae23-26dc25582cec",
          "text": "Deslocar-se à morada do cuidador escolhido",
          "type": "istar.Task",
          "x": 1311,
          "y": 661,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
          "text": "Entregar Formulário",
          "type": "istar.Task",
          "x": 1551,
          "y": 680,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a3c4c5d7-53c6-4798-9925-6a82949ad366",
          "text": "Receber Declaração",
          "type": "istar.Task",
          "x": 1435,
          "y": 670,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
          "text": "Efetuar Pagamento",
          "type": "istar.Task",
          "x": 1213,
          "y": 680,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "9fa8ceaa-8ef1-4296-999b-10431f46e69c",
          "text": "Escrever Crítica",
          "type": "istar.Task",
          "x": 1550,
          "y": 926,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "text": "Avaliador",
      "type": "istar.Role",
      "x": 122,
      "y": 855,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "text": "NIF",
      "type": "istar.Resource",
      "x": 346,
      "y": 716,
      "customProperties": {
        "Description": ""
      },
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "text": "NIF validado",
      "type": "istar.Goal",
      "x": 508,
      "y": 722,
      "customProperties": {
        "Description": ""
      },
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2",
      "text": "Critica",
      "type": "istar.Resource",
      "x": 96,
      "y": 730,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "383e2470-0483-491a-b50d-494d5a4fbb7c",
      "text": "Critica",
      "type": "istar.Resource",
      "x": 96,
      "y": 730,
      "customProperties": {
        "Description": ""
      },
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    },
    {
      "id": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08",
      "text": "Entregar Animal",
      "type": "istar.Task",
      "x": 835,
      "y": 835,
      "customProperties": {
        "Description": ""
      },
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "91c2d369-6b4a-496f-8f49-7fd546a140e0",
      "text": "Declaração",
      "type": "istar.Task",
      "x": 995,
      "y": 568,
      "customProperties": {
        "Description": ""
      },
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "a3c4c5d7-53c6-4798-9925-6a82949ad366"
    },
    {
      "id": "a45ff141-8fae-4f5a-acf7-4b7bc848db16",
      "text": "Formulário de Registo",
      "type": "istar.Resource",
      "x": 1137,
      "y": 231,
      "customProperties": {
        "Description": ""
      },
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "db6a1667-de6b-41e1-9039-e9358fc8c25f",
      "text": "Animal",
      "type": "istar.Resource",
      "x": 1128,
      "y": 290,
      "customProperties": {
        "Description": ""
      },
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508",
      "text": "Efetuar Transferência",
      "type": "istar.Task",
      "x": 1072,
      "y": 361,
      "customProperties": {
        "Description": ""
      },
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    }
  ],
  "links": [
    {
      "id": "0d39f7d0-00e0-4f1d-ab3a-54d6120cc841",
      "type": "istar.DependencyLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11"
    },
    {
      "id": "5e35f349-247c-4af3-ac6e-69b21d097714",
      "type": "istar.DependencyLink",
      "source": "9a0a9b03-dcfe-4b5b-ad2b-01e7cf770f11",
      "target": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c"
    },
    {
      "id": "a229e16a-099e-4dc3-8915-a7e83f153783",
      "type": "istar.DependencyLink",
      "source": "b2daac5a-5316-4de3-a770-f7fc1cac4d2c",
      "target": "53fd3212-eb7f-48ac-91f4-016be7af1334"
    },
    {
      "id": "0be8cddd-e6ac-46ab-a95e-24e90bb3deb0",
      "type": "istar.DependencyLink",
      "source": "53fd3212-eb7f-48ac-91f4-016be7af1334",
      "target": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf"
    },
    {
      "id": "706f4cb3-d8d9-481a-a77e-f9dd36f25820",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2"
    },
    {
      "id": "7887efb9-4e0d-4f56-b9a2-a0a9b983477d",
      "type": "istar.DependencyLink",
      "source": "711c4fb9-a3b7-4e79-a328-4f0c65655bb2",
      "target": "bbfb9c75-71d1-408c-bd09-04d3030789d9"
    },
    {
      "id": "da3a317c-e69e-4dbb-b4d7-19b41833fd6e",
      "type": "istar.DependencyLink",
      "source": "88fa0c2b-36cb-4eaf-95d5-b7a166796a13",
      "target": "383e2470-0483-491a-b50d-494d5a4fbb7c"
    },
    {
      "id": "3ab0090c-a93a-4f7b-8a1a-77d960ad9dd6",
      "type": "istar.DependencyLink",
      "source": "383e2470-0483-491a-b50d-494d5a4fbb7c",
      "target": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130"
    },
    {
      "id": "549eaeb9-3881-48a0-9e28-e9a906b6ab00",
      "type": "istar.ContributionLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "help"
    },
    {
      "id": "c72c8696-dd4e-4517-b48c-7529c40d2d0e",
      "type": "istar.OrRefinementLink",
      "source": "61f7b3b0-d28c-482e-a630-cff2bcb73d55",
      "target": "d01ea420-83d2-440a-80ef-065de95ea200"
    },
    {
      "id": "f27fe4ac-b3cf-4e6d-b208-bb58508145c3",
      "type": "istar.AndRefinementLink",
      "source": "d01ea420-83d2-440a-80ef-065de95ea200",
      "target": "ce868adf-9e22-4bac-87f3-dbf8005f91ce"
    },
    {
      "id": "a99d99cb-176b-41f4-bc33-070651dec177",
      "type": "istar.IsALink",
      "source": "c1a9482c-0604-4e09-a5e7-419fd00747fe",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "08758600-5260-4285-b531-98c7761645e1",
      "type": "istar.IsALink",
      "source": "9ce67fba-5edd-42aa-b1cc-ea681cf6692d",
      "target": "6291d91a-a11b-43d2-a3d6-9273f069f3a3"
    },
    {
      "id": "028004dd-36c6-4cef-a0b1-dfd3b3042879",
      "type": "istar.AndRefinementLink",
      "source": "d3d860a6-571f-496a-97f9-42c29f04aa85",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "523184b8-16ce-476d-a041-c0ceb95f6577",
      "type": "istar.AndRefinementLink",
      "source": "a5a828a8-e179-4b2b-86ba-c1ced7924bdf",
      "target": "61f7b3b0-d28c-482e-a630-cff2bcb73d55"
    },
    {
      "id": "30e57569-b4cc-4759-baf4-e8db090ba7d5",
      "type": "istar.AndRefinementLink",
      "source": "47e92a65-f5dc-4dab-98a7-43de377aaf17",
      "target": "43a12b8c-9fc5-4085-a76d-50d084e02d4f"
    },
    {
      "id": "c07bd7ae-d94b-4b4a-85ff-b9239aec9727",
      "type": "istar.AndRefinementLink",
      "source": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "88f22346-79a4-47d4-a45a-9d983e093412",
      "type": "istar.AndRefinementLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "47e92a65-f5dc-4dab-98a7-43de377aaf17"
    },
    {
      "id": "b43bf8e4-d0f6-4914-8834-a613ed53fe65",
      "type": "istar.ContributionLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "ad3097c6-2c93-414c-bb09-636d8cfd1d64",
      "label": "hurt"
    },
    {
      "id": "808daa36-4fc9-4b69-a3f0-3a34edfe33e0",
      "type": "istar.AndRefinementLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "fa803668-be2d-4ba1-99c1-0b8586ab4e11",
      "type": "istar.AndRefinementLink",
      "source": "43a12b8c-9fc5-4085-a76d-50d084e02d4f",
      "target": "74428296-ebf9-4ad9-90e5-cc2da5dbf5e8"
    },
    {
      "id": "102838ee-66aa-4c3f-ae02-98bd790a7b50",
      "type": "istar.ContributionLink",
      "source": "ce868adf-9e22-4bac-87f3-dbf8005f91ce",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "77d95c00-e5b8-471f-9afe-4539bf42e224",
      "type": "istar.ContributionLink",
      "source": "bbfb9c75-71d1-408c-bd09-04d3030789d9",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "help"
    },
    {
      "id": "efb7601a-5276-43ed-9e2e-d7bac9da923f",
      "type": "istar.ContributionLink",
      "source": "db0f3e0e-b8a8-4c7b-be6b-1fe04ca93130",
      "target": "6422aaa1-581d-42be-b052-780206d80ead",
      "label": "hurt"
    },
    {
      "id": "f998b950-379f-45e7-83bb-e7dbbf9bb42c",
      "type": "istar.AndRefinementLink",
      "source": "17f40edb-4ab1-45d8-a82a-cc9a5c5bd778",
      "target": "7db97c1c-8d6e-47a6-a900-24a96bf256ad"
    },
    {
      "id": "d1aae658-a6fb-4669-9c33-f3cf778ac556",
      "type": "istar.AndRefinementLink",
      "source": "104f892f-54c8-47d5-84fe-b45ab3b1126d",
      "target": "7db97c1c-8d6e-47a6-a900-24a96bf256ad"
    },
    {
      "id": "f80855fc-a191-450a-b6f2-e1aee7517822",
      "type": "istar.AndRefinementLink",
      "source": "232176fc-5cfd-47eb-8739-1c45e85af606",
      "target": "76a24f3d-daaa-47ea-8376-8d033245d33b"
    },
    {
      "id": "6c26c260-a492-430a-a28b-11ae701e57e5",
      "type": "istar.AndRefinementLink",
      "source": "7db97c1c-8d6e-47a6-a900-24a96bf256ad",
      "target": "76a24f3d-daaa-47ea-8376-8d033245d33b"
    },
    {
      "id": "4a0e77ba-5056-4336-a43b-e0b08455c93e",
      "type": "istar.AndRefinementLink",
      "source": "76a24f3d-daaa-47ea-8376-8d033245d33b",
      "target": "7b65a7b0-7876-4bd2-ad21-124ed8867e87"
    },
    {
      "id": "6dbb90a1-eec5-4d24-b592-87bcbbd715f6",
      "type": "istar.AndRefinementLink",
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "18193dee-e9d9-4ddd-94a8-09f70a19855a",
      "type": "istar.AndRefinementLink",
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "7297cfb0-197d-4add-a63a-260fa44583c1",
      "type": "istar.AndRefinementLink",
      "source": "a3c4c5d7-53c6-4798-9925-6a82949ad366",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "2b2b41cc-49a0-44ee-b725-b10661a0734c",
      "type": "istar.AndRefinementLink",
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "3f121ca9-c712-4917-8625-d92e4b879097"
    },
    {
      "id": "745114cd-31e4-4005-909f-0164f6b486bb",
      "type": "istar.OrRefinementLink",
      "source": "9fa8ceaa-8ef1-4296-999b-10431f46e69c",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "8e8d8982-e974-4345-89e5-5935a20d15aa",
      "type": "istar.DependencyLink",
      "source": "e2697e75-4c49-4648-bf2e-48eb365f57f5",
      "target": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08"
    },
    {
      "id": "aa105482-cc48-4d40-b116-9b3a3fd6b3b2",
      "type": "istar.DependencyLink",
      "source": "cfefb6dc-67a8-48fd-ad0d-7e320c1dbe08",
      "target": "88ae1a01-7319-4519-ac20-658e964728e7"
    },
    {
      "id": "5ba1f582-de39-48f8-9d3f-5da615d51707",
      "type": "istar.DependencyLink",
      "source": "52e20b1b-5330-4d27-9c73-1bd37502fe80",
      "target": "91c2d369-6b4a-496f-8f49-7fd546a140e0"
    },
    {
      "id": "42a17176-bfc4-4ad5-a9e5-656bc7ce3d30",
      "type": "istar.DependencyLink",
      "source": "91c2d369-6b4a-496f-8f49-7fd546a140e0",
      "target": "a3c4c5d7-53c6-4798-9925-6a82949ad366"
    },
    {
      "id": "4c2e4f3e-c611-4742-ba4a-f70cff1a1cf9",
      "type": "istar.DependencyLink",
      "source": "45aacd89-84b5-430b-b94d-e26a84bc40e2",
      "target": "a45ff141-8fae-4f5a-acf7-4b7bc848db16"
    },
    {
      "id": "065cc0ac-e10d-45d9-b184-22a415773fc3",
      "type": "istar.DependencyLink",
      "source": "a45ff141-8fae-4f5a-acf7-4b7bc848db16",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "bd022e44-e1fe-47d8-9493-68e6fcdee3e3",
      "type": "istar.DependencyLink",
      "source": "7029f750-67f0-4468-ae23-26dc25582cec",
      "target": "db6a1667-de6b-41e1-9039-e9358fc8c25f"
    },
    {
      "id": "bf8f790b-82b0-4697-9381-2863227336a8",
      "type": "istar.DependencyLink",
      "source": "db6a1667-de6b-41e1-9039-e9358fc8c25f",
      "target": "d3d860a6-571f-496a-97f9-42c29f04aa85"
    },
    {
      "id": "03b10a10-172b-4efc-9216-2a5d8f8892ea",
      "type": "istar.DependencyLink",
      "source": "eb0c2088-80f3-4b8a-8706-c2530d4074be",
      "target": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508"
    },
    {
      "id": "e1bd8e64-ba34-4588-8b9c-db00469b823c",
      "type": "istar.DependencyLink",
      "source": "62fa99fe-8d1e-4d29-b5b1-3e2935c4a508",
      "target": "e3e3e283-77b0-4ec5-ab47-604ad20dda1d"
    }
  ],
  "display": {
    "ad3097c6-2c93-414c-bb09-636d8cfd1d64": {
      "width": 81.80621337890625,
      "height": 46.84869384765625
    },
    "7029f750-67f0-4468-ae23-26dc25582cec": {
      "width": 108.88333129882812,
      "height": 61
    },
    "3ab0090c-a93a-4f7b-8a1a-77d960ad9dd6": {
      "vertices": [
        {
          "x": 164,
          "y": 734
        },
        {
          "x": 276,
          "y": 618
        }
      ]
    },
    "aa105482-cc48-4d40-b116-9b3a3fd6b3b2": {
      "vertices": [
        {
          "x": 990,
          "y": 979
        },
        {
          "x": 1199,
          "y": 998
        },
        {
          "x": 1426,
          "y": 986
        }
      ]
    },
    "5ba1f582-de39-48f8-9d3f-5da615d51707": {
      "vertices": [
        {
          "x": 625,
          "y": 598
        },
        {
          "x": 703,
          "y": 610
        }
      ]
    },
    "42a17176-bfc4-4ad5-a9e5-656bc7ce3d30": {
      "vertices": [
        {
          "x": 1400,
          "y": 593
        },
        {
          "x": 1490,
          "y": 641
        }
      ]
    },
    "b2daac5a-5316-4de3-a770-f7fc1cac4d2c": {
      "collapsed": true
    },
    "9ce67fba-5edd-42aa-b1cc-ea681cf6692d": {
      "collapsed": true
    },
    "c1a9482c-0604-4e09-a5e7-419fd00747fe": {
      "collapsed": true
    },
    "88fa0c2b-36cb-4eaf-95d5-b7a166796a13": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Sat, 20 Nov 2021 19:27:43 GMT",
  "diagram": {
    "width": 2000,
    "height": 1649,
    "name": "Rationale View ( Cuidador )",
    "customProperties": {
      "Description": ""
    }
  }
}
