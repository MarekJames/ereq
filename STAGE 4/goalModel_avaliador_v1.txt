{
  "actors": [
    {
      "id": "509fbe7a-e173-41b2-92fc-bf4b3030eb3c",
      "text": "Avaliador",
      "type": "istar.Role",
      "x": 285,
      "y": 71,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "b9cbc6dc-2ab8-43d6-ac0c-b07efa40fa8d",
          "text": "Cuidador analisado",
          "type": "istar.Goal",
          "x": 462,
          "y": 80,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "37d419b5-d35b-4521-8d88-669b9e1dda9b",
          "text": "Login",
          "type": "istar.Task",
          "x": 451,
          "y": 361,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f4f3fa6b-1daf-44cf-b126-b181f9847fb9",
          "text": "Conta de avaliador criada",
          "type": "istar.Goal",
          "x": 716,
          "y": 71,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "2b74c77f-9ec4-4353-a35c-7248b806ccff",
          "text": "Registar",
          "type": "istar.Task",
          "x": 627,
          "y": 300,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "64a37c1e-658f-44ac-8654-b130f22ae14b",
          "text": "Sem erros",
          "type": "istar.Quality",
          "x": 372,
          "y": 439,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "acc9202e-dacb-4ee4-bcf5-fcfb9cfd82a5",
          "text": "Fácil e rapido",
          "type": "istar.Quality",
          "x": 698,
          "y": 374,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "448ed255-e4c8-46d9-8724-97d18727db35",
          "text": "Computador com internet",
          "type": "istar.Resource",
          "x": 330,
          "y": 351,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "9bb52df3-fab3-45b6-ac9e-3da4a086dc49",
          "text": "Comunicar com o administrador",
          "type": "istar.Task",
          "x": 703,
          "y": 219,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "22d7b981-8fbe-4ed8-aa90-3493788a3829",
          "text": "Adquirir permissões",
          "type": "istar.Task",
          "x": 698,
          "y": 145,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "d8ea0ed5-5324-470b-a5de-01e2378ccc79",
          "text": "Computador com internet",
          "type": "istar.Resource",
          "x": 742,
          "y": 303,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f108adfd-a8af-44bd-ae82-e35c8c671358",
          "text": "Procurar utilizando multi-filtros por um cuidador",
          "type": "istar.Task",
          "x": 439,
          "y": 267,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "630514d0-2798-4133-8eca-054f4a777aad",
          "text": "Escolher cuidador",
          "type": "istar.Task",
          "x": 452,
          "y": 213,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "3ce08736-8f65-4985-b147-0761b8ee6d86",
          "text": "Resposta rápida do servidor",
          "type": "istar.Quality",
          "x": 285,
          "y": 256,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b0ffda74-3a54-4f6a-9e32-57d5091ee42f",
          "text": "Atribuir selo de qualidade",
          "type": "istar.Task",
          "x": 402,
          "y": 149,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ebc6c742-078e-4b52-a6eb-e23d7a89ac5a",
          "text": "Banir cuidador",
          "type": "istar.Task",
          "x": 507,
          "y": 148,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    }
  ],
  "orphans": [],
  "dependencies": [],
  "links": [
    {
      "id": "797fd598-519b-44cb-aa08-cc5e994b520c",
      "type": "istar.AndRefinementLink",
      "source": "f108adfd-a8af-44bd-ae82-e35c8c671358",
      "target": "630514d0-2798-4133-8eca-054f4a777aad"
    },
    {
      "id": "97c9bd4e-de32-4959-9d4b-0fb803cbc93f",
      "type": "istar.AndRefinementLink",
      "source": "37d419b5-d35b-4521-8d88-669b9e1dda9b",
      "target": "f108adfd-a8af-44bd-ae82-e35c8c671358"
    },
    {
      "id": "6923c141-72f3-4482-9fae-52b34302dcba",
      "type": "istar.AndRefinementLink",
      "source": "2b74c77f-9ec4-4353-a35c-7248b806ccff",
      "target": "9bb52df3-fab3-45b6-ac9e-3da4a086dc49"
    },
    {
      "id": "d81553a4-ddd9-4021-99fd-ef296beee6a7",
      "type": "istar.AndRefinementLink",
      "source": "9bb52df3-fab3-45b6-ac9e-3da4a086dc49",
      "target": "22d7b981-8fbe-4ed8-aa90-3493788a3829"
    },
    {
      "id": "1f79a52f-4236-49b5-b9fd-6c4188fc036d",
      "type": "istar.AndRefinementLink",
      "source": "22d7b981-8fbe-4ed8-aa90-3493788a3829",
      "target": "f4f3fa6b-1daf-44cf-b126-b181f9847fb9"
    },
    {
      "id": "2aa2897b-9c59-4c2a-b1b3-8ab40de01eb7",
      "type": "istar.OrRefinementLink",
      "source": "b0ffda74-3a54-4f6a-9e32-57d5091ee42f",
      "target": "b9cbc6dc-2ab8-43d6-ac0c-b07efa40fa8d"
    },
    {
      "id": "c2498628-3f8f-4bf7-8602-5fd85d690e45",
      "type": "istar.OrRefinementLink",
      "source": "ebc6c742-078e-4b52-a6eb-e23d7a89ac5a",
      "target": "b9cbc6dc-2ab8-43d6-ac0c-b07efa40fa8d"
    },
    {
      "id": "8fa1eac9-c3f5-428b-a33e-cbd199eaa5bc",
      "type": "istar.AndRefinementLink",
      "source": "630514d0-2798-4133-8eca-054f4a777aad",
      "target": "b0ffda74-3a54-4f6a-9e32-57d5091ee42f"
    },
    {
      "id": "bc81fe5c-c3d7-44e2-bab6-5fd4d17e04b0",
      "type": "istar.AndRefinementLink",
      "source": "630514d0-2798-4133-8eca-054f4a777aad",
      "target": "ebc6c742-078e-4b52-a6eb-e23d7a89ac5a"
    },
    {
      "id": "3eb485dc-44c4-418e-814c-a2c940a2f518",
      "type": "istar.NeededByLink",
      "source": "448ed255-e4c8-46d9-8724-97d18727db35",
      "target": "37d419b5-d35b-4521-8d88-669b9e1dda9b"
    },
    {
      "id": "92aa91b2-2ed7-4354-869f-6d817e5704a2",
      "type": "istar.NeededByLink",
      "source": "d8ea0ed5-5324-470b-a5de-01e2378ccc79",
      "target": "2b74c77f-9ec4-4353-a35c-7248b806ccff"
    },
    {
      "id": "dd26d0f7-d42f-4183-a9c7-544dbacb0cf2",
      "type": "istar.ContributionLink",
      "source": "37d419b5-d35b-4521-8d88-669b9e1dda9b",
      "target": "64a37c1e-658f-44ac-8654-b130f22ae14b",
      "label": "help"
    },
    {
      "id": "b497afb9-6c52-4179-b640-89cbe97b7d6c",
      "type": "istar.ContributionLink",
      "source": "2b74c77f-9ec4-4353-a35c-7248b806ccff",
      "target": "acc9202e-dacb-4ee4-bcf5-fcfb9cfd82a5",
      "label": "make"
    },
    {
      "id": "97187ffd-a105-4b67-ace0-4a32d4b72614",
      "type": "istar.ContributionLink",
      "source": "f108adfd-a8af-44bd-ae82-e35c8c671358",
      "target": "3ce08736-8f65-4985-b147-0761b8ee6d86",
      "label": "make"
    }
  ],
  "display": {
    "f4f3fa6b-1daf-44cf-b126-b181f9847fb9": {
      "width": 92.88748168945312,
      "height": 47.412498474121094
    },
    "448ed255-e4c8-46d9-8724-97d18727db35": {
      "width": 102.88748168945312,
      "height": 45.82499694824219
    },
    "9bb52df3-fab3-45b6-ac9e-3da4a086dc49": {
      "width": 113.88748168945312,
      "height": 53.82499694824219
    },
    "22d7b981-8fbe-4ed8-aa90-3493788a3829": {
      "width": 123.88748168945312,
      "height": 53.82499694824219
    },
    "f108adfd-a8af-44bd-ae82-e35c8c671358": {
      "width": 115.38748168945312,
      "height": 72.82499694824219
    },
    "3ce08736-8f65-4985-b147-0761b8ee6d86": {
      "width": 106.91940307617188,
      "height": 68.3321762084961
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Fri, 19 Nov 2021 18:45:01 GMT",
  "diagram": {
    "width": 2000,
    "height": 1347,
    "name": "Avaliador",
    "customProperties": {
      "Description": ""
    }
  }
}