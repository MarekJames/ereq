{
  "actors": [
    {
      "id": "8068fc87-7b4f-4572-99c1-a1ecca6cf560",
      "text": "Dono de Animal",
      "type": "istar.Role",
      "x": 792,
      "y": 60,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "4045748a-9b54-4bf9-932e-4b31716829a4",
          "text": "Animal entregue",
          "type": "istar.Goal",
          "x": 908,
          "y": 77,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "560c430b-8678-4e0b-9860-635538e101df",
          "text": "Cuidador Encontrado",
          "type": "istar.Goal",
          "x": 818,
          "y": 168,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
          "text": "Pesquisa cuidador",
          "type": "istar.Task",
          "x": 802,
          "y": 247,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a65c6c0d-5daf-4adf-8200-27cceff2cf08",
          "text": "Pesquisa rápida",
          "type": "istar.Quality",
          "x": 810,
          "y": 343,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ac93ae66-88d9-4926-ae8c-d546f1e382a1",
          "text": "Preencher formulário",
          "type": "istar.Task",
          "x": 1015,
          "y": 168,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "56eda46a-e644-40e0-8e9a-fd7780ac9ebf",
          "text": "Preencher declaração",
          "type": "istar.Task",
          "x": 912,
          "y": 169,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "39632215-bc23-452e-a3eb-d83c37b07728",
          "text": "Pagar o serviço",
          "type": "istar.Task",
          "x": 1115,
          "y": 167,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "334016a6-6d44-4a59-a0e3-8850c066a7ac",
          "text": "Escrever Crítica",
          "type": "istar.Task",
          "x": 972,
          "y": 356,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "744f3bb4-0105-433a-b4f5-f88aaf0fb76f",
          "text": "Serviço avaliado",
          "type": "istar.Goal",
          "x": 918,
          "y": 263,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7bc3fa23-64a6-4ce4-b42f-8cb2abb0adf2",
          "text": "Metodo de Pagamento",
          "type": "istar.Resource",
          "x": 1043,
          "y": 65,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b054df33-858a-4602-b951-b6f77a6c2a0b",
          "text": "Seguro",
          "type": "istar.Quality",
          "x": 1296,
          "y": 71,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "165df4c5-d3a3-4a71-b32d-3e7f742801aa",
          "text": "Obter melhores Condições",
          "type": "istar.Quality",
          "x": 1048,
          "y": 254,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "afee7b8b-4a1f-499b-b141-793a7d88fb4b",
          "text": "Banco",
          "type": "istar.Resource",
          "x": 1142,
          "y": 105,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "7061b1f2-7197-432a-a62a-da5cb612ab9b",
      "text": "Veterinario",
      "type": "istar.Role",
      "x": 44,
      "y": 454,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "9d3ac02d-a7a5-4086-89d3-e07016376d3b",
      "text": "Avaliadores",
      "type": "istar.Role",
      "x": 350,
      "y": 409,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "8436ee25-e2ba-4c56-ab2d-7b8788116851",
          "text": "Qualidade garantida",
          "type": "istar.Goal",
          "x": 423,
          "y": 435,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "bed79074-4765-4b19-8245-5df8423a692c",
          "text": "Critica Avaliada",
          "type": "istar.Goal",
          "x": 421,
          "y": 494,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "565f1b8c-b9ae-4e8e-8779-6504276574ce",
          "text": "Utilizador Banido",
          "type": "istar.Goal",
          "x": 350,
          "y": 581,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "206fa453-9d90-4271-8660-a30511abd9a7",
          "text": "Selo de Qualidade Atribuido",
          "type": "istar.Goal",
          "x": 487,
          "y": 579,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "5b20e43b-66a3-4025-8b59-45d3ccffe6a5",
          "text": "Ler critica",
          "type": "istar.Task",
          "x": 427,
          "y": 659,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "595b5a00-ca2f-4353-9f08-25a3a6870b6f",
          "text": "Melhorar Serviço",
          "type": "istar.Quality",
          "x": 563,
          "y": 419,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "a6db86f5-90ed-41bf-a386-a88c3d8def88",
      "text": "Empresa",
      "type": "istar.Role",
      "x": 42,
      "y": 289,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "74784c31-8722-47c3-9a10-1e19e5ceeea1",
      "text": "Particular",
      "type": "istar.Role",
      "x": 50,
      "y": 98,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "c3a28a32-931b-40e1-89a6-8bc0c357d98b",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 180,
      "y": 93,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "0e521832-fe38-489e-9dab-574f9533071a",
          "text": "Publicitar Serviço",
          "type": "istar.Goal",
          "x": 377,
          "y": 109,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "1a0017cc-3baf-4c72-97c5-9075aac379ad",
          "text": "Aumentar Alcance",
          "type": "istar.Quality",
          "x": 499,
          "y": 93,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "fd7b9995-f056-4b5f-b7f2-1a7b58ba3784",
          "text": "Receber Critica Negativa",
          "type": "istar.Task",
          "x": 500,
          "y": 195,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f82a201a-f94c-47c3-aba6-0c5c14914af4",
          "text": "Receber Critica Positiva",
          "type": "istar.Task",
          "x": 600,
          "y": 194,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "f0f65348-f1d6-4cfd-98a9-e021f9e45889",
          "text": "Registar",
          "type": "istar.Task",
          "x": 380,
          "y": 183,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "ef89cc3c-2da8-4026-8ec2-f7332d679326",
          "text": "Fazer Dinheiro",
          "type": "istar.Goal",
          "x": 211,
          "y": 179,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "7a3beac0-5a0d-486c-81b4-0ef41e322e3b",
          "text": "Prestar Serviço",
          "type": "istar.Task",
          "x": 377,
          "y": 255,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "4d9f731e-ab3b-4ab5-8b17-ecbbe8f23069",
          "text": "Recebe Critica",
          "type": "istar.Task",
          "x": 551,
          "y": 265,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    }
  ],
  "orphans": [],
  "dependencies": [
    {
      "id": "9ab35572-f8ad-4ad2-906d-d19cad3e52f9",
      "text": "Website",
      "type": "istar.Resource",
      "x": 800,
      "y": 663,
      "customProperties": {
        "Description": ""
      },
      "source": "5b20e43b-66a3-4025-8b59-45d3ccffe6a5",
      "target": "334016a6-6d44-4a59-a0e3-8850c066a7ac"
    },
    {
      "id": "62a8244e-01fa-4181-b9cb-df0fe261ed61",
      "text": "Receber Animal",
      "type": "istar.Task",
      "x": 622,
      "y": 361,
      "customProperties": {
        "Description": ""
      },
      "source": "7a3beac0-5a0d-486c-81b4-0ef41e322e3b",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    }
  ],
  "links": [
    {
      "id": "d31615b6-ddc2-4bb4-831d-9fa9581513f5",
      "type": "istar.DependencyLink",
      "source": "5b20e43b-66a3-4025-8b59-45d3ccffe6a5",
      "target": "9ab35572-f8ad-4ad2-906d-d19cad3e52f9"
    },
    {
      "id": "88281f63-0843-4c89-a0f3-7a5d98f256b4",
      "type": "istar.DependencyLink",
      "source": "9ab35572-f8ad-4ad2-906d-d19cad3e52f9",
      "target": "334016a6-6d44-4a59-a0e3-8850c066a7ac"
    },
    {
      "id": "06d42610-247f-4f51-be9e-213df101e801",
      "type": "istar.AndRefinementLink",
      "source": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
      "target": "560c430b-8678-4e0b-9860-635538e101df"
    },
    {
      "id": "d530caeb-7f08-48d9-a62c-4df47155628a",
      "type": "istar.AndRefinementLink",
      "source": "560c430b-8678-4e0b-9860-635538e101df",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    },
    {
      "id": "edd4c163-b3bd-4d5e-a460-cb3bf448a417",
      "type": "istar.AndRefinementLink",
      "source": "56eda46a-e644-40e0-8e9a-fd7780ac9ebf",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    },
    {
      "id": "50ef5c35-fa10-4d27-9613-a16510f5992d",
      "type": "istar.AndRefinementLink",
      "source": "ac93ae66-88d9-4926-ae8c-d546f1e382a1",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    },
    {
      "id": "2721e36e-b87b-45da-b2fe-d467d815c143",
      "type": "istar.AndRefinementLink",
      "source": "39632215-bc23-452e-a3eb-d83c37b07728",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    },
    {
      "id": "9d0661ba-be39-4780-896e-91e152d6f20d",
      "type": "istar.AndRefinementLink",
      "source": "334016a6-6d44-4a59-a0e3-8850c066a7ac",
      "target": "744f3bb4-0105-433a-b4f5-f88aaf0fb76f"
    },
    {
      "id": "a6ba85ed-aaa5-41a3-898b-c540b242b760",
      "type": "istar.AndRefinementLink",
      "source": "bed79074-4765-4b19-8245-5df8423a692c",
      "target": "8436ee25-e2ba-4c56-ab2d-7b8788116851"
    },
    {
      "id": "a7660737-88a7-4c57-8225-3eceef3c4278",
      "type": "istar.OrRefinementLink",
      "source": "565f1b8c-b9ae-4e8e-8779-6504276574ce",
      "target": "bed79074-4765-4b19-8245-5df8423a692c"
    },
    {
      "id": "a6ba2b1f-03af-4678-8c68-f05540bb12bc",
      "type": "istar.OrRefinementLink",
      "source": "206fa453-9d90-4271-8660-a30511abd9a7",
      "target": "bed79074-4765-4b19-8245-5df8423a692c"
    },
    {
      "id": "c97f6098-9ce8-4e20-ac0d-5e7f5b7afeb5",
      "type": "istar.AndRefinementLink",
      "source": "5b20e43b-66a3-4025-8b59-45d3ccffe6a5",
      "target": "206fa453-9d90-4271-8660-a30511abd9a7"
    },
    {
      "id": "db82fb19-0859-456a-bacb-6e42da4e8f63",
      "type": "istar.AndRefinementLink",
      "source": "5b20e43b-66a3-4025-8b59-45d3ccffe6a5",
      "target": "565f1b8c-b9ae-4e8e-8779-6504276574ce"
    },
    {
      "id": "ef574439-c10a-4d8e-89cc-d2876ab915b6",
      "type": "istar.NeededByLink",
      "source": "7bc3fa23-64a6-4ce4-b42f-8cb2abb0adf2",
      "target": "39632215-bc23-452e-a3eb-d83c37b07728"
    },
    {
      "id": "ad2d9947-b00c-499f-9e26-54f232033b34",
      "type": "istar.ContributionLink",
      "source": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
      "target": "a65c6c0d-5daf-4adf-8200-27cceff2cf08",
      "label": "hurt"
    },
    {
      "id": "f7e36003-72b6-40ea-a240-59bcb9caf5e5",
      "type": "istar.ContributionLink",
      "source": "334016a6-6d44-4a59-a0e3-8850c066a7ac",
      "target": "165df4c5-d3a3-4a71-b32d-3e7f742801aa",
      "label": "make"
    },
    {
      "id": "9dfbf335-a434-49c3-846d-3a18cf45a4e1",
      "type": "istar.NeededByLink",
      "source": "afee7b8b-4a1f-499b-b141-793a7d88fb4b",
      "target": "39632215-bc23-452e-a3eb-d83c37b07728"
    },
    {
      "id": "368374ea-b0a0-467f-bc64-a4a6eb2d3a4c",
      "type": "istar.QualificationLink",
      "source": "b054df33-858a-4602-b951-b6f77a6c2a0b",
      "target": "39632215-bc23-452e-a3eb-d83c37b07728"
    },
    {
      "id": "f5eb9fb3-ae90-4bf8-9b9c-8a676c7e64a4",
      "type": "istar.ContributionLink",
      "source": "7bc3fa23-64a6-4ce4-b42f-8cb2abb0adf2",
      "target": "b054df33-858a-4602-b951-b6f77a6c2a0b",
      "label": "help"
    },
    {
      "id": "19be8364-fa42-48aa-8e72-3cab1743b6e3",
      "type": "istar.ContributionLink",
      "source": "afee7b8b-4a1f-499b-b141-793a7d88fb4b",
      "target": "b054df33-858a-4602-b951-b6f77a6c2a0b",
      "label": "help"
    },
    {
      "id": "fd661c50-5856-48a9-baf6-c73b78ffe174",
      "type": "istar.QualificationLink",
      "source": "1a0017cc-3baf-4c72-97c5-9075aac379ad",
      "target": "0e521832-fe38-489e-9dab-574f9533071a"
    },
    {
      "id": "185ce902-5721-4de7-9836-efd463dd4cb4",
      "type": "istar.AndRefinementLink",
      "source": "0e521832-fe38-489e-9dab-574f9533071a",
      "target": "ef89cc3c-2da8-4026-8ec2-f7332d679326"
    },
    {
      "id": "07bdca84-7fb8-467f-86ba-b77e2eccf773",
      "type": "istar.ContributionLink",
      "source": "fd7b9995-f056-4b5f-b7f2-1a7b58ba3784",
      "target": "1a0017cc-3baf-4c72-97c5-9075aac379ad",
      "label": "hurt"
    },
    {
      "id": "5cba3d23-3155-47ff-a9d4-d7902f1678a6",
      "type": "istar.ContributionLink",
      "source": "f82a201a-f94c-47c3-aba6-0c5c14914af4",
      "target": "1a0017cc-3baf-4c72-97c5-9075aac379ad",
      "label": "help"
    },
    {
      "id": "e848aecc-6e19-4fa1-86af-fb098b85d2b0",
      "type": "istar.AndRefinementLink",
      "source": "7a3beac0-5a0d-486c-81b4-0ef41e322e3b",
      "target": "ef89cc3c-2da8-4026-8ec2-f7332d679326"
    },
    {
      "id": "3f78b986-044c-4cfe-b505-ef776b61d8c2",
      "type": "istar.IsALink",
      "source": "74784c31-8722-47c3-9a10-1e19e5ceeea1",
      "target": "c3a28a32-931b-40e1-89a6-8bc0c357d98b"
    },
    {
      "id": "d7bfb036-d6d7-45f7-9f10-4f145eadaab6",
      "type": "istar.IsALink",
      "source": "a6db86f5-90ed-41bf-a386-a88c3d8def88",
      "target": "c3a28a32-931b-40e1-89a6-8bc0c357d98b"
    },
    {
      "id": "9fc92022-5d98-4ba0-91b5-fb01030a2167",
      "type": "istar.ContributionLink",
      "source": "565f1b8c-b9ae-4e8e-8779-6504276574ce",
      "target": "595b5a00-ca2f-4353-9f08-25a3a6870b6f",
      "label": "help"
    },
    {
      "id": "ae327bb8-def2-482d-8610-091f616558ef",
      "type": "istar.ContributionLink",
      "source": "206fa453-9d90-4271-8660-a30511abd9a7",
      "target": "595b5a00-ca2f-4353-9f08-25a3a6870b6f",
      "label": "help"
    },
    {
      "id": "6232a89d-4037-4992-8f58-dd7f93724a6b",
      "type": "istar.QualificationLink",
      "source": "165df4c5-d3a3-4a71-b32d-3e7f742801aa",
      "target": "744f3bb4-0105-433a-b4f5-f88aaf0fb76f"
    },
    {
      "id": "d61b748d-21a5-47b7-93fe-108667a70921",
      "type": "istar.QualificationLink",
      "source": "595b5a00-ca2f-4353-9f08-25a3a6870b6f",
      "target": "8436ee25-e2ba-4c56-ab2d-7b8788116851"
    },
    {
      "id": "e3fba7d0-9d1c-473e-8dea-d9de8458771f",
      "type": "istar.AndRefinementLink",
      "source": "f0f65348-f1d6-4cfd-98a9-e021f9e45889",
      "target": "ef89cc3c-2da8-4026-8ec2-f7332d679326"
    },
    {
      "id": "ed8eba21-c1ec-439a-b282-a4e67f142450",
      "type": "istar.OrRefinementLink",
      "source": "fd7b9995-f056-4b5f-b7f2-1a7b58ba3784",
      "target": "4d9f731e-ab3b-4ab5-8b17-ecbbe8f23069"
    },
    {
      "id": "1966b162-13d7-48d4-a273-42f18af9335a",
      "type": "istar.OrRefinementLink",
      "source": "f82a201a-f94c-47c3-aba6-0c5c14914af4",
      "target": "4d9f731e-ab3b-4ab5-8b17-ecbbe8f23069"
    },
    {
      "id": "fe0bcb45-ec80-472f-920e-c98d53d51f76",
      "type": "istar.DependencyLink",
      "source": "7a3beac0-5a0d-486c-81b4-0ef41e322e3b",
      "target": "62a8244e-01fa-4181-b9cb-df0fe261ed61"
    },
    {
      "id": "1dfde150-f6cb-41a4-812a-d57f8f8fdd93",
      "type": "istar.DependencyLink",
      "source": "62a8244e-01fa-4181-b9cb-df0fe261ed61",
      "target": "4045748a-9b54-4bf9-932e-4b31716829a4"
    }
  ],
  "display": {
    "88281f63-0843-4c89-a0f3-7a5d98f256b4": {
      "vertices": [
        {
          "x": 1003,
          "y": 606
        }
      ]
    },
    "368374ea-b0a0-467f-bc64-a4a6eb2d3a4c": {
      "vertices": [
        {
          "x": 1340,
          "y": 184
        }
      ]
    },
    "f5eb9fb3-ae90-4bf8-9b9c-8a676c7e64a4": {
      "vertices": [
        {
          "x": 1224,
          "y": 79
        }
      ]
    },
    "19be8364-fa42-48aa-8e72-3cab1743b6e3": {
      "vertices": [
        {
          "x": 1286,
          "y": 139
        }
      ]
    },
    "5cba3d23-3155-47ff-a9d4-d7902f1678a6": {
      "vertices": [
        {
          "x": 631,
          "y": 158
        }
      ]
    },
    "9fc92022-5d98-4ba0-91b5-fb01030a2167": {
      "vertices": [
        {
          "x": 422,
          "y": 436
        }
      ]
    },
    "ae327bb8-def2-482d-8610-091f616558ef": {
      "vertices": [
        {
          "x": 589,
          "y": 555
        }
      ]
    },
    "1966b162-13d7-48d4-a273-42f18af9335a": {
      "vertices": [
        {
          "x": 627,
          "y": 243
        },
        {
          "x": 623,
          "y": 250
        }
      ]
    },
    "fe0bcb45-ec80-472f-920e-c98d53d51f76": {
      "vertices": [
        {
          "x": 477,
          "y": 366
        },
        {
          "x": 519,
          "y": 377
        }
      ]
    },
    "1dfde150-f6cb-41a4-812a-d57f8f8fdd93": {
      "vertices": [
        {
          "x": 737,
          "y": 301
        },
        {
          "x": 769,
          "y": 56
        },
        {
          "x": 900,
          "y": 41
        }
      ]
    },
    "7061b1f2-7197-432a-a62a-da5cb612ab9b": {
      "collapsed": true
    },
    "a6db86f5-90ed-41bf-a386-a88c3d8def88": {
      "collapsed": true
    },
    "74784c31-8722-47c3-9a10-1e19e5ceeea1": {
      "collapsed": true
    }
  },
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Thu, 18 Nov 2021 22:58:51 GMT",
  "diagram": {
    "width": 2000,
    "height": 1666,
    "customProperties": {
      "Description": ""
    }
  }
}
