{
  "actors": [
    {
      "id": "86c76e6f-ac57-4a37-8ad1-f4b60a48faf4",
      "text": "Cuidador",
      "type": "istar.Role",
      "x": 399,
      "y": 24,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "93af7c13-bb15-4102-83ca-db7f5f7d32fa",
          "text": "Serviços publicitados",
          "type": "istar.Goal",
          "x": 477,
          "y": 38,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "8068fc87-7b4f-4572-99c1-a1ecca6cf560",
      "text": "Dono de Animal",
      "type": "istar.Role",
      "x": 798,
      "y": 18,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "4045748a-9b54-4bf9-932e-4b31716829a4",
          "text": "Animal entregue",
          "type": "istar.Goal",
          "x": 986,
          "y": 18,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "560c430b-8678-4e0b-9860-635538e101df",
          "text": "Cuidador Encontrado",
          "type": "istar.Goal",
          "x": 914,
          "y": 109,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
          "text": "Pesquisa cuidador",
          "type": "istar.Task",
          "x": 798,
          "y": 184,
          "customProperties": {
            "Description": ""
          }
        },
        {
          "id": "a65c6c0d-5daf-4adf-8200-27cceff2cf08",
          "text": "Pesquisa rápida",
          "type": "istar.Quality",
          "x": 842,
          "y": 264,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "7061b1f2-7197-432a-a62a-da5cb612ab9b",
      "text": "Veterinario",
      "type": "istar.Role",
      "x": 855,
      "y": 370,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "9d3ac02d-a7a5-4086-89d3-e07016376d3b",
      "text": "Avaliadores",
      "type": "istar.Role",
      "x": 384,
      "y": 408,
      "customProperties": {
        "Description": ""
      },
      "nodes": [
        {
          "id": "8436ee25-e2ba-4c56-ab2d-7b8788116851",
          "text": "Qualidade garantida",
          "type": "istar.Goal",
          "x": 454,
          "y": 431,
          "customProperties": {
            "Description": ""
          }
        }
      ]
    },
    {
      "id": "ce7d8a12-0ed7-4253-91f4-1a004fe1c606",
      "text": "Banco",
      "type": "istar.Actor",
      "x": 798,
      "y": 641,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "a6db86f5-90ed-41bf-a386-a88c3d8def88",
      "text": "Empresa",
      "type": "istar.Role",
      "x": 93,
      "y": 230,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    },
    {
      "id": "74784c31-8722-47c3-9a10-1e19e5ceeea1",
      "text": "Particular",
      "type": "istar.Role",
      "x": 130,
      "y": 83,
      "customProperties": {
        "Description": ""
      },
      "nodes": []
    }
  ],
  "orphans": [],
  "dependencies": [],
  "links": [
    {
      "id": "a6822226-3cb2-4bb5-a217-6c7f01c8e53d",
      "type": "istar.IsALink",
      "source": "a6db86f5-90ed-41bf-a386-a88c3d8def88",
      "target": "86c76e6f-ac57-4a37-8ad1-f4b60a48faf4"
    },
    {
      "id": "b2ee112e-cab5-4368-9c08-6c3b7a4689f9",
      "type": "istar.IsALink",
      "source": "74784c31-8722-47c3-9a10-1e19e5ceeea1",
      "target": "86c76e6f-ac57-4a37-8ad1-f4b60a48faf4"
    },
    {
      "id": "1c41a35f-7c8e-4040-9831-e60a2e877baa",
      "type": "istar.AndRefinementLink",
      "source": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
      "target": "560c430b-8678-4e0b-9860-635538e101df"
    },
    {
      "id": "18244f61-20a8-4ccf-ac72-7c47c5afbdb4",
      "type": "istar.ContributionLink",
      "source": "b0afe880-55f5-43f1-8031-6b9b51e028fa",
      "target": "a65c6c0d-5daf-4adf-8200-27cceff2cf08",
      "label": "break"
    }
  ],
  "display": {},
  "tool": "pistar.2.0.0",
  "istar": "2.0",
  "saveDate": "Thu, 18 Nov 2021 00:16:13 GMT",
  "diagram": {
    "width": 2000,
    "height": 1576,
    "customProperties": {
      "Description": ""
    }
  }
}
